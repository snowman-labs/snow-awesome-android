# Snow Awesome Android

O objetivo desta lista é reunir referências de conteúdos técnicos como, bibliotecas, documentações, artigos, cursos, tutoriais, vídeos, livros, podcasts e quaisquer outros conteúdos pertinentes ao aprendizado e aprofundamento de cada tema. 

## Como contribuir

Sugira uma referência apenas se você já a utilizou antes, é importante manter o conteúdo sob curadoria para que essa lista não se torne apenas um *bookmark* de coisas interessantes que você quer salvar para o futuro.

As contribuições devem ser feitas através de *Merge Request* com uma breve explicação do conteúdo e motivação. 

As referências devem seguir o seguinte formato:

```
* [Nome](Link) - Breve explicação, author, ou informação relevante. #tipo 
```

Exemplos:

* [Kotlinlang.org](https://kotlinlang.org) - Documentação oficial de Kotlin. #doc


---

## Linguagem

* [Kotlinlang.org](https://kotlinlang.org) - Documentação oficial de Kotlin. #doc

## Boas Práticas
## Metaprogramação
## Algoritmos
## Design Patterns
## REST/OpenAPI
## GraphQL
## Sistemas Distribuídos
## Sistemas orientados a Eventos
## Arquitetura
## LGPD
## Gestão de Configuração
## Gestão de Dependências
## Gestão de Credenciais e Certificados
## Gestão de Releases
## Gestão de Infraestrutura
## Gestão de crises
## Métricas
## Monitoramento
## CI/CD
## Testes
## Manutenção de software legado
## Análise Estática de Código
## Análise de Vulnerabilidades
## UI/UX
## Design System
,